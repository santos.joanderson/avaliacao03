#!/bin/bash

vidas=$1
vidas_dragao=$2
nome="Pombudo"

while true;do
# É preciso instalar o cowsay

cowsay -f dragon "Nome do dragon é $nome"
echo ""
echo "=== Menu do jogo principal ==="
echo "1 - Atacar "
echo "2 - Fugir"
echo "3 - Info"
echo "4 - Sair"
echo ""

read -p "Escolha uma das opções acima: " op
echo ""

case $op in
1)echo "Voce atacou o $nome e causou (100 pontos)"
vidas_dragao=$((vidas_dragao - 100))

	if [ $vidas_dragao -gt 0 ];then
		echo "$nome te atacou (10 pontos de dano)"
		vidas=$((vidas - 10))
	else
		echo "Voce venceu o $nome"
		break
	fi
	;;
2)chances=$(($RANDOM % 2))
	if [ $chances -eq 0 ];then
		echo "Voce fugiu do $nome"
		break
	else
		echo "Voce foi derrotado pelo $nome"
		break
	fi;;

3)
echo ""
echo "== Informações =="
echo "Sua vida: $vidas"
echo "Vida do Dragão: $vidas_dragao"
echo ""	;;

4)echo "Fim de game..."
	exit 0;;
esac

echo ""

done



