#!/bin/bash

user=$1


while true;do
echo "1 -> Verifica se o usuário existe"
echo "2 -> Verifica se o usuário está logado na máquina"
echo "3 -> Lista os arquivos da pasta home do usuário"
echo "4 -> Sair"

read -p "Digite a opção que deseja: " n

if [ $n == 1 ]; then
  if id $user >/dev/null 2>$1; then
	 echo ""
	 echo "O usuário $user existe"
	 echo ""
  else
	 echo "O Usuário não existe"
	 echo who | grep $user
	 echo ls /$user/home
	 exit 0
  fi
fi


if [ $n == 2 ]; then
  who | grep "$user"
  if [ $? -eq 0 ];then
	 echo ""
	 echo "O usuário $user está logado na máquina"
	 echo ""
  else
	 echo "O Usuário não está logado"
  fi
fi


if [ $n == 3 ]; then
  ls -l /home/$user
fi


if [ $n == 4 ]; then
  exit 0
fi


 
done
